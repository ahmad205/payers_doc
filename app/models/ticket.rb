class Ticket < ApplicationRecord
    has_many :ticket_replies
    validates_presence_of :number
    validates_presence_of :title
    validates_presence_of :content
    enum status: { Open: 0, OnHold: 1, Answered: 2, Closed: 3 }
    enum evaluation: { "Very Bad": 1, "Bad": 2, "Good": 3, "Very Good": 4 , "Excellent":5 }
    belongs_to :user ,:foreign_key => "user_id"
end
