class CardsCategoriesController < ApplicationController
  before_action :set_cards_category, only: [:show, :edit, :update, :destroy]

  # GET list of Cards Categories and display it
  # @return [id] card category unique ID.
  # @return [card_value] card category value (Unique).
  # @return [active] card category display status (1 for show , 0 for hide).
  # @return [order] card category unique order (high order for high card category value).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @cards_categories = CardsCategory.all
  end

  # GET a spacific card  category and display it
  # @param [Integer] id card category unique ID.
  # @return [card_value] card category value (Unique).
  # @return [active] card category display status (1 for show , 0 for hide).
  # @return [order] card category unique order (high order for high card category value).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET list of a Displayed Cards Categories ordered Desc
  # @return [id] card category unique ID.
  # @return [card_value] card category value (Unique).
  # @return [active] card category display status (1 for show).
  # @return [order] card category unique order (high order for high card category value).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def display_cards_categories
    @categories = CardsCategory.where(:active => 1).all.order(card_value: :desc)
  end

  # GET a new Card Category
  # @param [Integer] id card category unique ID (Created automatically).
  # @param [Integer] card_value card category value (Unique).
  # @param [Integer] active card category display status (1 for show , 0 for hide).
  # @param [Integer] order card category unique order (high order for high card category value).
  # @param [Date] created_at Date created.
  # @param [Date] updated_at Date Updated.
  def new
    @cards_category = CardsCategory.new
  end

  # POST a new Card Category and save it
  # @return [id] card category unique ID (Created automatically).
  # @return [card_value] card category value (Unique).
  # @return [active] card category display status (1 for show , 0 for hide).
  # @return [order] card category unique order (high order for high card category value).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @cards_category = CardsCategory.new(cards_category_params)

    respond_to do |format|
      if @cards_category.save
        format.html { redirect_to @cards_category, notice: 'Cards category was successfully created.' }
        format.json { render :show, status: :created, location: @cards_category }
      else
        format.html { render :new }
        format.json { render json: @cards_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET an existing card category to edit params
  # @param [Integer] card_value card category value (Unique).
  # @param [Integer] active card category display status (1 for show , 0 for hide).
  # @param [Integer] order card category unique order (high order for high card category value)
  def edit
  end

  # Change an existing card params(card_value,active,order)
  # @return [id] card category unique ID.
  # @return [card_value] card category value (Unique).
  # @return [active] card category display status (1 for show , 0 for hide).
  # @return [order] card category unique order (high order for high card category value).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @cards_category.update(cards_category_params)
        format.html { redirect_to @cards_category, notice: 'Cards category was successfully updated.' }
        format.json { render :show, status: :ok, location: @cards_category }
      else
        format.html { render :edit }
        format.json { render json: @cards_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE an existing card category
  # @param [Integer] id card category unique ID.
  # @return [card_value] card category value (Unique).
  # @return [active] card category display status (1 for show , 0 for hide).
  # @return [order] card category unique order.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def destroy
    @cards_category.destroy
    respond_to do |format|
      format.html { redirect_to cards_categories_url, notice: 'Cards category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cards_category
      @cards_category = CardsCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cards_category_params
      params.require(:cards_category).permit(:card_value, :active, :order)
    end
end
