class CardsLogsController < ApplicationController
  before_action :set_cards_log, only: [:show]


  # GET list of cards logs and display it
  # @return [id] card log unique ID (Created automatically).
  # @return [user_id] card log unique user id.
  # @return [card_id] card unique id.
  # @return [action_type] card log type (1 for created ,2 for charged ,3 for deleted).
  # @return [ip] card log user interface ip.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @cards_logs = CardsLog.all
  end

  # GET a spacific card log and display it
  # @param [Integer] id card unique ID (Created automatically).
  # @return [user_id] card log unique user id.
  # @return [card_id] card unique id.
  # @return [action_type] card log type (1 for created ,2 for charged ,3 for deleted).
  # @return [ip] card log user interface ip.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cards_log
      @cards_log = CardsLog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cards_log_params
      params.require(:cards_log).permit(:user_id, :action_type, :ip, :card_id)
    end
end
