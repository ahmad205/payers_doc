module Api
  module V1
    class CardsController < ApplicationController
       before_action :set_card, only: [:show, :edit, :update, :destroy]
       skip_before_action :verify_authenticity_token

      
      # GET /cards
      def index
        @cards = Card.all
        respond_to do |format|
          format.json { render json: @cards }
      end

      # =begin
      # @api {get} /api/v1/cards 1-Request cards List
      # @apiVersion 0.3.0
      # @apiName GetCard
      # @apiGroup Cards
      # @apiExample Example usage:
      # curl -i http://localhost:3000/api/v1/cards
      # @apiSuccess {Number} id card unique ID.
      # @apiSuccess {String} number card unique number.
      # @apiSuccess {Number} value card value.
      # @apiSuccess {Number} status card status.
      # @apiSuccess {Number} status card status (1 for Active, 2 for Charged , 3 for Pending).
      # @apiSuccess {Number} user_id card for spacific user id.
      # @apiSuccess {Number} card_type card type (1 for spacific user, 2 for spacific country).
      # @apiSuccess {Date} expired_at  card expired Date.
      # @apiSuccess {Number} invoice_id card invoice ID.
      # @apiSuccess {Date} created_at  Date created.
      # @apiSuccess {Date} updated_at  Date Updated.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # [
      #      {
      #         "id": 5,
      #         "number": "c35hl9zisa42u6rp",
      #         "value": 150,
      #         "status": 3,
      #         "expired_at": "2018-07-24",
      #         "card_type": 1,
      #         "country_id": 0,
      #         "user_id": 2,
      #         "invoice_id": 0,
      #         "created_at": "2018-07-24T12:50:56.749Z",
      #         "updated_at": "2018-07-24T12:51:18.567Z"
      #     },
      #     {
      #         "id": 6,
      #         "number": "vtszhr3wfyedcbju",
      #         "value": 200,
      #         "status": 1,
      #         "expired_at": "2022-06-25",
      #         "card_type": 2,
      #         "country_id": 1,
      #         "user_id": 0,
      #         "invoice_id": 0,
      #         "created_at": "2018-07-24T13:24:23.665Z",
      #         "updated_at": "2018-07-24T13:24:23.665Z"
      #     }
      # ]
      # @apiError MissingToken invalid Token.
      # @apiErrorExample Error-Response:
      # HTTP/1.1 400 Bad Request
      #   {
      #     "error": "Missing token"
      #   }
      # =end
      end

      # GET /cards/:id
      def show
        # =begin
        # @api {get} /api/v1/cards/{:id} 2-Request Specific Card
        # @apiVersion 0.3.0
        # @apiName GetSpecificCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards/1
        # @apiParam {Number} id Card ID.
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {String} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Number} status card status (1 for Active, 2 for Charged , 3 for Pending).
        # @apiSuccess {Number} user_id card for spacific user id.
        # @apiSuccess {Number} card_type card type (1 for spacific user, 2 for spacific country).
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 1,
        #         "number": "fckpzo7h3gtb94xq",
        #         "value": 100,
        #         "status": 2,
        #         "expired_at": "2022-05-20",
        #         "card_type": 1,
        #         "country_id": 0,
        #         "user_id": 1,
        #         "invoice_id": 0,
        #         "created_at": "2018-07-24T13:24:23.665Z",
        #         "updated_at": "2018-07-24T13:24:23.665Z"
        #     }
        # @apiError CardNotFound The id of the Card was not found. 
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CardNotFound"
        #   }
        # @apiError MissingToken invalid token.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

        respond_to do |format|
          format.json { render json: @card }
        end

      end

      # GET /cards/new
      def new
        @card = Card.new
      end

      # GET /cards/:id/edit
      def edit
      end

      # POST /cards
      def create

        # =begin
        # @api {post} /api/v1/cards 3-Create a new Card
        # @apiVersion 0.3.0
        # @apiName PostCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/cards \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "value": 300,
        #   "status": 1,
        #   "card_type": 2,
        #   "country_id": 1,
        #   "expired_at": "2022-07-26"
        # }'
        # @apiParam {Number} number card number must be 16 digit (Created automatically).
        # @apiParam {Number} value card value.
        # @apiParam {Number} status card status (1 for Active, 2 for Charged , 3 for Pending).
        # @apiParam {Number} card_type card type (1 for spacific user, 2 for spacific country).
        # @apiParam {Number} user_id card for spacific user id.
        # @apiParam {Number} country_id card for spacific country id.
        # @apiParam {Date} expired_at  card expired Date.
        # @apiParam {Number} invoice_id card invoice ID. 
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 7,
        #         "number": 1178702457640936,
        #         "value": 500,
        #         "status": 2,
        #         "expired_at": "2022-05-20",
        #         "user_id": 1,
        #         "invoice_id": 0,
        #         "created_at": "2018-07-25T13:24:23.665Z",
        #         "updated_at": "2018-07-25T13:24:23.665Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError ExistingNumber card number has already been taken.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 Existing Number
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError InvalidNumber card number not equal 16 digit.
        # @apiErrorExample Error-Response3:
        # HTTP/1.1 Invalid Number
        #   {
        #     "error": "must be 16 digit long"
        #   }
        # =end

        @card = Card.new(card_params)
        @user = params[:card][:user_id]
        @card_status = params[:card][:status].to_i
        @card_type = params[:card][:card_type].to_i
        if @user != "0"
          @userdata = User.where("account_number LIKE ? OR email LIKE ?", "%#{@user}%","%#{@user}%").first
          @card.user_id = @userdata.id.to_i
          ActiveRecord::Base.transaction do
            @user_wallet = UserWallet.where(user_id: @userdata.id.to_i).first
            @new_balance = @user_wallet.amount.to_f + params[:card][:value].to_f
            @user_notification_setting = NotificationsSetting.where(user_id: @userdata.id.to_i).first
            if @card_status == 2
              @money_status = 1
              @user_wallet.update(amount:@new_balance)
              @smstext = "New Card was successfully created and balance with value of #{params[:card][:value]} USD was add to your wallet by payers admin"
            elsif @card_status == 1
              @money_status = 1
              @smstext = "New active card with value of #{params[:card][:value]} USD was successfully created to your account by payers admin"
            elsif @card_status == 3
              @money_status = 0
              @smstext = "New Pending card with value of #{params[:card][:value]} USD was successfully created to your account by payers admin"
            end
            @money_operation = MoneyOp.create(:optype => 4,:amount => params[:card][:value] ,:payment_gateway => "Payers" ,:status => @money_status,:payment_date => DateTime.now,:user_id => @userdata.id.to_i)
            @card.operation_id = @money_operation.opid
            access_notification(@userdata,@smstext)
          end
        else
          @card.user_id = 0
        end
        respond_to do |format|
          if @card.save
            CardsLog.create(:user_id => 1,:action_type => 1,:ip => request.remote_ip,:card_id => @card.id)
            format.json { render json: @card, status: :created, location: @card }
          else
            format.json { render json: @card.errors, status: :unprocessable_entity }
          end
        end

      end

      # PATCH/PUT /cards/:id
      def update

        # =begin
        # @api {put} /api/v1/cards/{:id} 4-Update an existing Card
        # @apiVersion 0.3.0
        # @apiName PutCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/v1/cards/6 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "value": 400,
        #   "status": 2,
        #   "expired_at": "2018-07-26",
        #   "user_id": 3
        # }'
        # @apiParam {Number} value card value.
        # @apiParam {Number} status card status.
        # @apiParam {Date} expired_at  card expired Date.
        # @apiParam {Number} user_id card user ID. 
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        #     {
        #         "id": 6,
        #         "number": "fckpzo7h3gtb94xq",
        #         "value": 200,
        #         "status": 2,
        #         "expired_at": "2022-05-20",
        #         "user_id": 1,
        #         "invoice_id": 0,
        #         "created_at": "2018-07-25T13:24:23.665Z",
        #         "updated_at": "2018-07-25T13:24:23.665Z"
        #     }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError CardNotFound The id of the Card was not found. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CardNotFound"
        #   }
        # =end

        respond_to do |format|
          if @card.update(card_params)

            if params[:card][:status].to_i == 2
              CardsLog.create(:user_id => 1,:action_type => 2,:ip => request.remote_ip,:card_id => @card.id)
            end
            format.json { render json: @card, status: :ok, location: @card }
          else
            format.json { render json: @card.errors, status: :unprocessable_entity }
          end
        end

      end

      def cards_status

        # =begin
        # @api {get} /api/v1/cards_status?id={:id} 6-Suspend a Specific Card
        # @apiVersion 0.3.0
        # @apiName SuspendCard
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards_status?id=22
        # @apiParam {Number} id card unique ID.
        # @apiSuccess {Number} status card status(1 for enable & 4 for suspend).
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "status": 1,
        #   "id": 22,
        #   "number": "gos3ncliadx1jr54",
        #   "value": 200,
        #   "expired_at": "2018-07-31",
        #   "card_type": 1,
        #   "country_id": 0,
        #   "user_id": 1,
        #   "invoice_id": 0,
        #   "created_at": "2018-07-28T10:07:53.960Z",
        #   "updated_at": "2018-07-29T08:29:33.484Z"
        # }
        # @apiError CardNotFound The ID of this Card was not found. 
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "Card Not Found"
        #   }
        # =end

        @card_status = Card.where(:id => params[:id].to_i).first
        flash[:error]= "Card Not Found"
        respond_to do |format|
          if @card_status

            if @card_status.status ==  4
              if @card_status.update(:status => 1)
                format.json { render json: @card_status }
              end
            else
              if @card_status.update(:status => 4)
                format.json { render json: @card_status }
              end
            end

          else
            format.json { render json: flash }
          end

        end
    
      end

      def search

        # =begin 
        # @api {get} /api/v1/search?&search={:search_word}&search_user={:user_id}&status={:status}&searchdatefrom={:expired_at_from}&searchdateto={:expired_at_to} 5-Search for an existing Card
        # @apiVersion 0.3.0
        # @apiName GetCardSearch
        # @apiGroup Cards
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/search?=&search=100&search_user=1&status=1&searchdatefrom=2018-07-24&searchdateto=2018-07-25
        # @apiParam {Number} number card number.
        # @apiParam {Number} value card value.
        # @apiParam {Number} search_word card search number or value.
        # @apiParam {Number} status card status(Active ,Charged ,pending ,disabled).
        # @apiParam {Number} user_id card User ID.
        # @apiParam {Date} expired_at  card expired Date.
        # @apiParam {Date} expired_at_from  card search expired Date from.
        # @apiParam {Date} expired_at_to  card search expired Date to.
        # @apiParam {Number} invoice_id card invoice ID. 
        # @apiSuccess {Number} id card unique ID.
        # @apiSuccess {Number} number card unique number.
        # @apiSuccess {Number} value card value.
        # @apiSuccess {Number} status card status.
        # @apiSuccess {Date} expired_at  card expired Date.
        # @apiSuccess {Number} invoice_id card invoice ID.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 2,
        #   "number": "mp01qwtcnvoy8iud",
        #   "value": 120,
        #   "status": 1,
        #   "expired_at": "2018-07-25",
        #   "card_type": 1,
        #   "country_id": 0,
        #   "user_id": 1,
        #   "invoice_id": 0,
        #   "created_at": "2018-07-24T12:43:16.492Z",
        #   "updated_at": "2018-07-25T14:01:18.051Z"
        # }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError NoResults There are no results for your search. 
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 404 No Results
        #   {
        #     "error": "NoResults"
        #   }
        # =end

        @search = params[:search].delete('-')
        @searchdatefrom = params[:searchdatefrom]
        @searchdateto = params[:searchdateto]
        @status = params[:status]
        @search_user = params[:search_user]
    
        @Cards = Card.all
        @Cards = @Cards.where("cards.expired_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
        @Cards = @Cards.where("cards.expired_at <= ? ", @searchdateto)   if  @searchdateto.present?
        @Cards = @Cards.where("cards.expired_at <= ? AND cards.expired_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
        @Cards = @Cards.where(["number LIKE  ? OR value = ?","%#{@search}%",@search]).distinct.all if  @search.present?
        @Cards = @Cards.where("cards.status = ? ", @status)   if  @status.present?
        @Cards = @Cards.where("cards.user_id = ? ", @search_user)   if  @search_user.present?

        respond_to do |format|
            format.json { render json: @Cards }
        end
    
      end
    
      def searchpost
      end

      # =begin
      # @api {post} /api/v1/admin_add_note 7-Admin Send Note
      # @apiVersion 0.3.0
      # @apiName AdminAddNote
      # @apiGroup Cards
      # @apiExample Example usage:
      # curl -X POST \
      # http://localhost:3000/api/v1/admin_add_note \
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #   "value": 300,
      #   "status": 1,
      #   "card_type": 2,
      #   "expired_at": "2022-07-26"
      # }'
      # @apiParam {String} admin_note admin text note.
      # @apiParam {String} operation_id card operation unique ID.
      # @apiSuccess {String} admin_note admin text note.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # {
      #   "admin_note": "Message Was Not Sent to user",
      #   "operation_id": PY593RHKG74W,
      # }
      # @apiError MassageWasNotSent something went wrong while sending this message. 
      # @apiErrorExample Error-Response:
      #   {
      #     "error": "Message Was Not Sent to user"
      #   }
      # =end
      def admin_add_note
        @note = params[:admin_add_note][:admin_note]
        @operation_id = params[:admin_add_note][:operation_id]
        @operation = MoneyOp.where(opid: @operation_id).first
        @operation.update(admin_note: @note)
        @user = User.where(id: @operation.user_id).first
        access_notification(@user,@note)
        flash[:error] = "Message Was Not Sent to user"
        
        respond_to do |format|
          if access_notification(@user,@note)
            format.json { render json: @note }
          else
            format.json { render json: flash }      
          end
        end

      end
    

      # =begin
      # @api {post} /api/v1/send_card 8-Send Card
      # @apiVersion 0.3.0
      # @apiName SendCard
      # @apiGroup Cards
      # @apiExample Example usage:
      # curl -X POST \
      # http://localhost:3000/api/v1/send_card \
      # -H 'cache-control: no-cache' \
      # -H 'content-type: application/json' \
      # -d '{
      #   "card_id": 10,
      #   "card_value": 100,
      #   "user_email": "usereamil@gmail.com",
      #   "card_number": "a90o-l7b6-hwrd-yj1e",
      #   "admin_msg": "hello from payers",
      # }'
      # @apiParam {Number} card_value value card value.
      # @apiParam {Number} card_id value card unique id.
      # @apiParam {String} card_number card unique number.
      # @apiParam {String} user_email receiver user email.
      # @apiParam {String} admin_msg message sent with user email.
      # @apiSuccess {String} smstext message sent to user.
      # @apiSuccessExample Success-Response:
      # HTTP/1.1 200 OK
      # {
      #   "admin_note": "Message Was Not Sent to user",
      # }
      # @apiError CardWasNotSent something went wrong while sending this card. 
      # @apiErrorExample Error-Response:
      #   {
      #     "error": "Card Was Not Sent to user"
      #   }
      # =end
      def send_card
        @user_email = params[:send_card][:user_email]
        @card_number = params[:send_card][:card_number]
        @card_value = params[:send_card][:card_value]
        @admin_msg = params[:send_card][:admin_msg]
        @card_id = params[:send_card][:card_id].to_i
        @card = Card.where(id: @card_id).first
        @card.update(note: @admin_msg)
        @smstext = "#{@admin_msg}
        Card Number is : #{@card_number}
        Card value is : #{@card_value} USD"
        EmailNotification.email_notification_setting(user_mail:@user_email,subject:'Recharge Wallet Balance',text:@smstext)
        flash[:error] = "Card Was Not Sent to user"
        respond_to do |format|
          if sendmail
            format.json { render json: @smstext }
          else
            format.json { render json: flash }      
          end
        end
      end

      # DELETE /cards/:id
      def destroy
        @card.destroy
        CardsLog.create(:user_id => 1,:action_type => 3,:ip => request.remote_ip,:card_id => @card.id)
        respond_to do |format|
          format.json { head :no_content }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_card
          @card = Card.find(params[:id])
        end

        def access_notification(user,smstext)

          @user = user
          @smstext = smstext
          @user_notification_setting = NotificationsSetting.where(user_id: @user.id).first
          Notification.create(user_id: @user.id ,title: "Recharge Wallet Balance", description: @smstext , notification_type: @user_notification_setting.money_transactions)
          if @user_notification_setting.money_transactions == 3
            SMSNotification.sms_notification_setting(@user.telephone,@smstext)
            SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
            EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext)
          elsif @user_notification_setting.money_transactions == 2
            SMSNotification.sms_notification_setting(@user.telephone,@smstext)
            SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
          elsif @user_notification_setting.money_transactions == 1
            EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext)
          end
          
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def card_params
          params.require(:card).permit(:number, :value, :status, :expired_at, :user_id, :invoice_id, :card_type ,:country_id)
        end

    end

  end

end
