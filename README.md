# Payers
payers is an online bank that enables customers to trade their money easily.

# Getting Started
We will begin by explaining all parts of the website and the technology used in it and the importance of each part.

# Prerequisites

* Ruby version

 we use ruby version 2.5.1 on rails version 5.2.1 .

* Gems

 gem 'rails-i18n' version 5.1.1 to customize and control the locales of the app.

 gem 'rspec-rails' version 3.8.0 to make an effective test in some functions.

 gem 'mailgun-ruby' version 1.1.10 to help us in our api connection with the mailgun service.

 gem 'country_select' version 3.1.1 to make it easy to choose a specific country from all world countries.

 gem 'clearance' Used for Authentication

 gem 'mini_magick'  Used for ActiveStorage variant

 gem 'device_detector' Used to detect devise details

 gem 'maxminddb' Used to read IP details

 gem 'active_model_otp' Used to implement Google's MFA authenticator

 gem 'rqrcode' Used to implement QR Code

 gem 'jwt' Used for Authentice user via token

 gem 'shoulda-matchers' ~> 3.1 Test 

 gem 'factory_bot_rails' ~> 4.0 Test

 gem 'faker' Test

 gem 'database_cleaner' Test

# Main Programming Parts

* Sign Up

 Admin can add new admins. 

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#add_admin-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#add_admin_post-instance_method

* Sign In

 Admins can sign in using Valid email and password.

 Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#create-instance_method

* confirm sign in via two factor authentication

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#get_two_factor-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#send_confirmation_email-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#post_two_factor-instance_method


* Send email to Reset Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#create-instance_method

* Reset Admin's Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#update-instance_method


* UnLock Admin account

 UnLock account after Number of failed attempts.

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Authentication-unlockaccount

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#unlockaccount-instance_method

* Show Admin profile 

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#show-instance_method

* Edit admin profile

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#edit-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#update-instance_method

* Choose another two factor authentication 

Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#change_confirmation_code-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/AdminsController#confirm_google_code-instance_method

* Show Admin Log

 Code documentation Link: http://0.0.0.0:8808/docs/AdminWatchdogsController#admin_log-instance_method

* Show Admin's Active Sessions

API documentation link:   https://payers-userpanel.herokuapp.com/api/index.html#api-Log-Getactivesessions

 Code documentation Link: http://0.0.0.0:8808/docs/AdminLoginsController#admin_active_sessions-instance_method

* Show National ID Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/NationalidVerificationsController

* Show Address Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/AddressVerificationsController

* Show Selfie Verification

Code documentation Link: http://0.0.0.0:8808/docs/SelfieVerificationsController

* Create New Country

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-postCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#new-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#create-instance_method

* Delete Country

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-DeleteCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#destroy-instance_method

* Show Country Details

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#show-instance_method

* List All Countries

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountries

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#index-instance_method

* Edit Country Data

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-PutCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#edit-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#update-instance_method

* List All Users

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-GetUsers

Code documentation Link: http://0.0.0.0:8808/docs/UsersController#index-instance_method

* Show user profile 

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-GetUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#show-instance_method

* Edit user profile

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-PutUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#edit-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#update-instance_method

* Sign Out

 Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#destroy-instance_method

 * Addresses

It contains a list of users addresses and the user must have at least one default address.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/AddressesController

API documentation link https://payers.herokuapp.com/#api-Addresses

* Cards

It contains a list of money cards used by the customer to charge his wallet balance in our website.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/CardsController

API documentation link https://payers.herokuapp.com/#api-Cards

* Cards Categories

This is the front page of cards that appear to the user to choose from the card to be charged.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/CardsCategoriesController

API documentation link https://payers.herokuapp.com/#api-Cards_Categories

* Cards Logs

This part contains all the operations performed on the cards and every card status.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/CardsLogsController

API documentation link https://payers.herokuapp.com/#api-Cards_Logs

* Media

This contain a list of api connections with social media (facebook,twitter,...).

Source code documentation link https://payers-card-doc.herokuapp.com/docs/MediaController

* Money Ops

It contains and control all the money operations that are carried out on the user account (withdraw,transfer,recharge balance).

Source code documentation link https://payers-card-doc.herokuapp.com/docs/MoneyOpsController

API documentation link https://payers.herokuapp.com/#api-Money_Operations

* Notifications

Contains the notifications that appear to the customer as a result of all the transactions and actions taken on his account.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/NotificationsController

API documentation link https://payers.herokuapp.com/#api-Notifications

* Notifications Settings

Make the customer able to control the notifications that appear to him from all sections of the website.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/NotificationsSettingsController

API documentation link https://payers.herokuapp.com/#api-Notifications_Settings

* User Wallets

It contains a list of all users wallets and their balances in our website.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/UserWalletsController

API documentation link https://payers.herokuapp.com/#api-Users_Wallets

* Wallets Transfer Ratios

It control the transfer balance operation between users (min_transfer,max_transfer).

Source code documentation link https://payers-card-doc.herokuapp.com/docs/WalletsTransferRatiosController

* Pages

It contains all the external interface pages and a sitemap to this app.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/PagesController

API documentation link https://payers.herokuapp.com/#api-Pages

* SMS Logs

It contains all SMSs sent to users from our app.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/SmsLogsController

API documentation link https://payers.herokuapp.com/#api-SMS_Logs

* Uploads

Control and edit every picture in the app and save it using active storage.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/UploadsController

* Translations

Control and edit all the locales file this app and save it in our database.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/TranslationsController

API documentation link https://payers.herokuapp.com/#api-Translations

# Test Code

This application also contains a test code in some important functions especially financial functions.

* User Wallets Test

Tests the process of creating wallets and transferring money between users and ensuring that no mistakes are made.

* Pages Test

Tests the process of create and delete pages without any mistake.

* Cards Test

Test the validation of some card parameters (card number must be equal 16 digits and letters).

* (Create,Authenticate,list all) User Test

Tests the process of create , authenticate and get list of all users without any mistake.

* Country Test

Tests the process of create and get list of all countries as well as test display and edit every country details.

# Todo

* delete registeration functions from admin

* delete some columns from admin and user models:

admin: language - status - secret_code - account_number - telephone - account_currency - country_id - invitation_code 

user: auth_token - roleid

* Merge between admin and  money_op , media , translation and upload models.