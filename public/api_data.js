define({ "api": [
  {
    "type": "Delete",
    "url": "/api/v1/addresses{:id}",
    "title": "7-Delete an existing address",
    "version": "0.3.0",
    "name": "DeleteAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X DELETE \\\nhttp://localhost:3000/api/v1/addresses/4 \\",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    \"success\": \"Address was successfully destroyed\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AddressNotFound",
            "description": "<p>The id of the address was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"AddressNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "Get",
    "url": "/api/v1/delete_user_addresses?{:user_id}",
    "title": "8-DELETE all addresses of a specific user",
    "version": "0.3.0",
    "name": "DeleteUserAddresses",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/delete_user_addresses?1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "user_id",
            "description": "<p>user ID than you want to delete all his addresses.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  },\n  {\n      \"id\": 5,\n      \"user_id\": 1,\n      \"address\": \"جده شارع خالد ابن الوليد\",\n      \"country\": \"SA\",\n      \"governorate\": \"جده\",\n      \"city\": \"الدخيله\",\n      \"street\": \"شارع خالد ابن الوليد\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T11:15:30.747Z\",\n      \"updated_at\": \"2018-09-12T11:15:30.747Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>There are no results for this user id.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 User Not Found\n  {\n    \"error\": \"This user was not found in our database\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/addresses",
    "title": "1-Request addresses List",
    "version": "0.3.0",
    "name": "GetAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/addresses",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  },\n  {\n      \"id\": 2,\n      \"user_id\": 2,\n      \"address\": \"المنصوره طلخا دقهليه\",\n      \"country\": \"EG\",\n      \"governorate\": \"دقهلية\",\n      \"city\": \"طلخا\",\n      \"street\": \"شارع البحر الاعظم\",\n      \"default_address\": true,\n      \"created_at\": \"2018-09-12T10:54:22.468Z\",\n      \"updated_at\": \"2018-09-12T11:28:17.326Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/search_address?&search={:search}&search_user={:user_id}&address_status={:default_address}&searchdatefrom={:created_at_from}&searchdateto={:created_at_to}",
    "title": "6-Search for an existing Address",
    "version": "0.3.0",
    "name": "GetAddressSearch",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/search_address?=&search=100&search_user=1&address_status=1&searchdatefrom=2018-09-11&searchdateto=2018-09-13",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search",
            "description": "<p>search word that you want to find(address,country,governorate,city,street).</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "searchdatefrom",
            "description": "<p>search created at date from.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "searchdateto",
            "description": "<p>search created at date to.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "address_status",
            "description": "<p>search default address status than you want(1 for Defualt , 0 for Not Default).</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "search_user",
            "description": "<p>search user ID than you want.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoResults",
            "description": "<p>There are no results for your search.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 No Results\n  {\n    \"error\": \"There are no results for your search.\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/addresses/{:id}",
    "title": "2-Request Specific Address",
    "version": "0.3.0",
    "name": "GetSpecificAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/addresses/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"user_id\": 1,\n  \"address\": \"New Damiatta , el-mahgob,service high\",\n  \"country\": \"EG\",\n  \"governorate\": \"Damiatta\",\n  \"city\": \"New Damiatta\",\n  \"street\": \"el-mahgob\",\n  \"default_address\": false,\n  \"created_at\": \"2018-09-12T10:48:29.946Z\",\n  \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AddressNotFound",
            "description": "<p>The id of the address was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"AddressNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "get",
    "url": "/api/v1/user_addresses",
    "title": "3-Request Specific User Addresses",
    "version": "0.3.0",
    "name": "GetSpecificUserAddresses",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_addresses",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"address\": \"New Damiatta , el-mahgob,service high\",\n      \"country\": \"EG\",\n      \"governorate\": \"Damiatta\",\n      \"city\": \"New Damiatta\",\n      \"street\": \"el-mahgob\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T10:48:29.946Z\",\n      \"updated_at\": \"2018-09-12T10:48:29.946Z\"\n  },\n  {\n      \"id\": 5,\n      \"user_id\": 1,\n      \"address\": \"جده شارع خالد ابن الوليد\",\n      \"country\": \"SA\",\n      \"governorate\": \"جده\",\n      \"city\": \"الدخيله\",\n      \"street\": \"شارع خالد ابن الوليد\",\n      \"default_address\": false,\n      \"created_at\": \"2018-09-12T11:15:30.747Z\",\n      \"updated_at\": \"2018-09-12T11:15:30.747Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The id of the user was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"UserNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "post",
    "url": "/api/v1/addresses",
    "title": "4-Create a new address",
    "version": "0.3.0",
    "name": "PostAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/addresses \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"user_id\": 3,\n  \"address\": \"mansoura,talkha\",\n  \"country\": \"EG\",\n  \"governorate\": \"Daqahliyah\",\n  \"city\": \"mansoura\",\n  \"street\": \"talkha,om kalthom street\",\n  \"default_address\": false\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 7,\n        \"user_id\": 3,\n        \"address\": \"mansoura,talkha\",\n        \"country\": \"EG\",\n        \"governorate\": \"Daqahliyah\",\n        \"city\": \"mansoura\",\n        \"street\": \"talkha,om kalthom street\",\n        \"default_address\": false,\n        \"created_at\": \"2018-09-12T11:15:30.747Z\",\n        \"updated_at\": \"2018-09-12T11:15:30.747Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAddress",
            "description": "<p>address must be more or equal than 5 letters.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Invalid Address\n  {\n    \"error\": \"must be more or equal than 5 letters\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "Put",
    "url": "/api/v1/addresses{:id}",
    "title": "5-Update an existing address",
    "version": "0.3.0",
    "name": "PutAddress",
    "group": "Addresses",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/addresses/7 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"address\": \"mansoura,talkha\",\n  \"country\": \"EG\",\n  \"governorate\": \"Daqahliyah\",\n  \"city\": \"mansoura\",\n  \"street\": \"talkha,om kalthom street\",\n  \"default_address\": true\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>address unique ID (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>the detailed address.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>the user address country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "governorate",
            "description": "<p>the user address governorate.expired_at  card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>the user address city.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "street",
            "description": "<p>the user address street.</p>"
          },
          {
            "group": "Success 200",
            "type": "Boolean",
            "optional": false,
            "field": "default_address",
            "description": "<p>defualt address for a user(true , false).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 7,\n        \"user_id\": 3,\n        \"address\": \"mansoura,talkha\",\n        \"country\": \"EG\",\n        \"governorate\": \"Daqahliyah\",\n        \"city\": \"mansoura\",\n        \"street\": \"talkha,om kalthom street\",\n        \"default_address\": true,\n        \"created_at\": \"2018-09-12T11:15:30.747Z\",\n        \"updated_at\": \"2018-09-12T13:14:21.335Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "AddressNotFound",
            "description": "<p>The id of the address was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidAddress",
            "description": "<p>address must be more or equal than 5 letters.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"AddressNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Address\n  {\n    \"error\": \"must be more or equal than 5 letters\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/addresses_controller.rb",
    "groupTitle": "Addresses"
  },
  {
    "type": "post",
    "url": "/affilate_programs/",
    "title": "Send Invitation Email",
    "version": "0.3.0",
    "name": "postAffilateProgram",
    "group": "AffilateProgram",
    "description": "<p>Send Invitation Email.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the invited user.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:       ",
        "content": "curl -X POST  http://localhost:3000/api/v1/send_invitation_email -H 'cache-control: no-cache' -H 'content-type: application/json' -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -d '{\"email\": \"engamira333@gmail.com\"}'",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"result\": \"true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>Only authenticated users can send invitation.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingData",
            "description": "<p>Email can not be empty.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"result : false\"\n   \"error\": \"Not Authenticated\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"result : false\"\n   \"error\": \"MissingData\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/affilate_programs_controller.rb",
    "groupTitle": "AffilateProgram"
  },
  {
    "type": "post",
    "url": "/authentication/",
    "title": "authenticate User",
    "version": "0.3.0",
    "name": "AuthenticateUser",
    "group": "Authentication",
    "description": "<p>User Authenticate.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's Password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -X POST -d email=\"amira.elfayome@servicehigh.com\" -d password=\"Amira123456\" http://localhost:3000/api/v1/auth_user",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>User's-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "auth_token",
            "description": "<p>User's authentication token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Invalid Email/Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Invalid Email/Password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/authentication_controller.rb",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/getEditToken",
    "title": "",
    "version": "0.3.0",
    "name": "AuthenticateUser",
    "group": "Authentication",
    "description": "<p>User Temporary Token.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's Password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -X POST -d email=\"amira.elfayome@servicehigh.com\" -d password=\"Amira123456\" http://localhost:3000/api/v1/getEditToken",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>User's-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Temporary_token",
            "description": "<p>User's Temporary token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Invalid Email/Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Invalid Email/Password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/authentication_controller.rb",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/unlockaccount/",
    "title": "unlock user account",
    "version": "0.3.0",
    "name": "unlockaccount",
    "group": "Authentication",
    "description": "<p>UnLock account after Number of failed attempts.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User's token to unlock account</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -X POST -d email=\"engamira333@gmail.com\" -d token=\"65f152a6\" http://localhost:3000/api/v1/unlockaccount",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing",
            "description": "<p>wrong email.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing2",
            "description": "<p>wrong code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Invalid Email\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Invalid Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/authentication_controller.rb",
    "groupTitle": "Authentication"
  },
  {
    "type": "get",
    "url": "/api/bank_accounts",
    "title": "1-Request bank accounts List",
    "version": "0.3.0",
    "name": "GetBank",
    "group": "Banks",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/bank_accounts",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Bank unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "bank_name",
            "description": "<p>Bank name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "branche_name",
            "description": "<p>Branche name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "swift_code",
            "description": "<p>Bank swift code.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>Bank country.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_name",
            "description": "<p>Name of the owner of the acount.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_number",
            "description": "<p>Bank account number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n        \"id\": 1,\n        \"bank_name\": \"بنك مصر\",\n        \"branche_name\": \"بنك مصر الدولي\",\n        \"swift_code\": \"47859654\",\n        \"country\": \"111\",\n        \"account_name\": \"omar\",\n        \"account_number\": \"123\",\n        \"created_at\": \"2018-09-05T11:07:13.000Z\",\n        \"updated_at\": \"2018-09-05T11:07:13.000Z\"\n    },\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/bank_accounts_controller.rb",
    "groupTitle": "Banks"
  },
  {
    "type": "get",
    "url": "/api/bank_accounts/{:id}",
    "title": "3-Request Specific Bank account",
    "version": "0.3.0",
    "name": "GetSpecificBank",
    "group": "Banks",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/bank_accounts/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Bank ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Bank unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "bank_name",
            "description": "<p>Bank name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "branche_name",
            "description": "<p>Branche name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "swift_code",
            "description": "<p>Bank swift code.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>Bank country.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_name",
            "description": "<p>Name of the owner of the acount.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_number",
            "description": "<p>Bank account number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n      \"id\": 1,\n      \"bank_name\": \"بنك مصر\",\n      \"branche_name\": \"بنك مصر الدولي\",\n      \"swift_code\": \"47859654\",\n      \"country\": \"111\",\n      \"account_name\": \"omar\",\n      \"account_number\": \"123\",\n      \"created_at\": \"2018-09-05T11:07:13.000Z\",\n      \"updated_at\": \"2018-09-05T11:07:13.000Z\"\n     }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BankAccountNotFound",
            "description": "<p>The id of the Bank was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"BankAccountNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/bank_accounts_controller.rb",
    "groupTitle": "Banks"
  },
  {
    "type": "post",
    "url": "/api/bank_accounts",
    "title": "2-Create a new bank account",
    "version": "0.3.0",
    "name": "PostBank",
    "group": "Banks",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/bank_accounts \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"bank_name\": \"البنك العربي الافريقي\",\n  \"branche_name\": \"البنك العربي الافريقي\",\n  \"swift_code\": \"78965125\",\n  \"country\": \"456\",\n  \"account_name\": \"omar\",\n  \"account_number\": \"7896423556\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "bank_name",
            "description": "<p>Bank name.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "branche_name",
            "description": "<p>Branche name.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "swift_code",
            "description": "<p>Bank swift code.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>Bank country.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account_name",
            "description": "<p>Name of the owner of the acount.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account_number",
            "description": "<p>Bank account number.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Bank unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "bank_name",
            "description": "<p>Bank name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "branche_name",
            "description": "<p>Branche name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "swift_code",
            "description": "<p>Bank swift code length must be 8 or 11.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>Bank country should be 3 characters.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_name",
            "description": "<p>Name of the owner of the acount.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_number",
            "description": "<p>Bank account number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 6,\n        \"bank_name\": \"البنك العربي الافريقي\",\n        \"branche_name\": \"البنك العربي الافريقي\",\n        \"swift_code\": \"78965125\",\n        \"country\": \"456\",\n        \"account_name\": \"omar\",\n        \"account_number\": \"7896423556\",\n        \"created_at\": \"2018-09-05T12:17:13.000Z\",\n        \"updated_at\": \"2018-09-05T12:17:13.000Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidSwiftCode",
            "description": "<p>bank swift code is invalid.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCountry",
            "description": "<p>bank country not equal 3 characters.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Invalid Swift Code\n  {\n    \"error\": \"is invalid\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Country\n  {\n    \"error\": \"is the wrong length (should be 3 characters)\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/bank_accounts_controller.rb",
    "groupTitle": "Banks"
  },
  {
    "type": "put",
    "url": "/api/bank_accounts/{:id}",
    "title": "4-Update an existing Bank",
    "version": "0.3.0",
    "name": "PutBank",
    "group": "Banks",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/bank_accounts/6 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"country\": \"777\",\n  \"account_name\": \"omar aransa\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>Bank country.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "account_name",
            "description": "<p>Name of the owner of the acount.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Bank unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "bank_name",
            "description": "<p>Bank name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "branche_name",
            "description": "<p>Branche name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "swift_code",
            "description": "<p>Bank swift code length must be 8 or 11.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "country",
            "description": "<p>Bank country should be 3 characters.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_name",
            "description": "<p>Name of the owner of the acount.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "account_number",
            "description": "<p>Bank account number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 6,\n        \"country\": \"777\",\n        \"account_name\": \"omar aransa\",\n        \"swift_code\": \"78965125\",\n        \"bank_name\": \"البنك العربي الافريقي\",\n        \"branche_name\": \"البنك العربي الافريقي\",\n        \"account_number\": \"7896423556\",\n        \"created_at\": \"2018-09-05T12:17:13.000Z\",\n        \"updated_at\": \"2018-09-05T12:40:43.000Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "BankNotFound",
            "description": "<p>The id of the Bank was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"BankNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/bank_accounts_controller.rb",
    "groupTitle": "Banks"
  },
  {
    "type": "post",
    "url": "/api/v1/admin_add_note",
    "title": "7-Admin Send Note",
    "version": "0.3.0",
    "name": "AdminAddNote",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/admin_add_note \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"value\": 300,\n  \"status\": 1,\n  \"card_type\": 2,\n  \"expired_at\": \"2022-07-26\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "admin_note",
            "description": "<p>admin text note.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "operation_id",
            "description": "<p>card operation unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "admin_note",
            "description": "<p>admin text note.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"admin_note\": \"Message Was Not Sent to user\",\n  \"operation_id\": PY593RHKG74W,\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MassageWasNotSent",
            "description": "<p>something went wrong while sending this message.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"error\": \"Message Was Not Sent to user\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/cards",
    "title": "1-Request cards List",
    "version": "0.3.0",
    "name": "GetCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card for spacific user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_type",
            "description": "<p>card type (1 for spacific user, 2 for spacific country).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n        \"id\": 5,\n        \"number\": \"c35hl9zisa42u6rp\",\n        \"value\": 150,\n        \"status\": 3,\n        \"expired_at\": \"2018-07-24\",\n        \"card_type\": 1,\n        \"country_id\": 0,\n        \"user_id\": 2,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-24T12:50:56.749Z\",\n        \"updated_at\": \"2018-07-24T12:51:18.567Z\"\n    },\n    {\n        \"id\": 6,\n        \"number\": \"vtszhr3wfyedcbju\",\n        \"value\": 200,\n        \"status\": 1,\n        \"expired_at\": \"2022-06-25\",\n        \"card_type\": 2,\n        \"country_id\": 1,\n        \"user_id\": 0,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-24T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-24T13:24:23.665Z\"\n    }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/search?&search={:search_word}&search_user={:user_id}&status={:status}&searchdatefrom={:expired_at_from}&searchdateto={:expired_at_to}",
    "title": "5-Search for an existing Card",
    "version": "0.3.0",
    "name": "GetCardSearch",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/search?=&search=100&search_user=1&status=1&searchdatefrom=2018-07-24&searchdateto=2018-07-25",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card number.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "search_word",
            "description": "<p>card search number or value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status(Active ,Charged ,pending ,disabled).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card User ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at_from",
            "description": "<p>card search expired Date from.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at_to",
            "description": "<p>card search expired Date to.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 2,\n  \"number\": \"mp01qwtcnvoy8iud\",\n  \"value\": 120,\n  \"status\": 1,\n  \"expired_at\": \"2018-07-25\",\n  \"card_type\": 1,\n  \"country_id\": 0,\n  \"user_id\": 1,\n  \"invoice_id\": 0,\n  \"created_at\": \"2018-07-24T12:43:16.492Z\",\n  \"updated_at\": \"2018-07-25T14:01:18.051Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoResults",
            "description": "<p>There are no results for your search.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 No Results\n  {\n    \"error\": \"NoResults\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/cards/{:id}",
    "title": "2-Request Specific Card",
    "version": "0.3.0",
    "name": "GetSpecificCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card for spacific user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_type",
            "description": "<p>card type (1 for spacific user, 2 for spacific country).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 1,\n        \"number\": \"fckpzo7h3gtb94xq\",\n        \"value\": 100,\n        \"status\": 2,\n        \"expired_at\": \"2022-05-20\",\n        \"card_type\": 1,\n        \"country_id\": 0,\n        \"user_id\": 1,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-24T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-24T13:24:23.665Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The id of the Card was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CardNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "post",
    "url": "/api/v1/cards",
    "title": "3-Create a new Card",
    "version": "0.3.0",
    "name": "PostCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/cards \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"value\": 300,\n  \"status\": 1,\n  \"card_type\": 2,\n  \"country_id\": 1,\n  \"expired_at\": \"2022-07-26\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card number must be 16 digit (Created automatically).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status (1 for Active, 2 for Charged , 3 for Pending).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_type",
            "description": "<p>card type (1 for spacific user, 2 for spacific country).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card for spacific user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "country_id",
            "description": "<p>card for spacific country id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 7,\n        \"number\": 1178702457640936,\n        \"value\": 500,\n        \"status\": 2,\n        \"expired_at\": \"2022-05-20\",\n        \"user_id\": 1,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-25T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-25T13:24:23.665Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingNumber",
            "description": "<p>card number has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidNumber",
            "description": "<p>card number not equal 16 digit.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Existing Number\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Number\n  {\n    \"error\": \"must be 16 digit long\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "put",
    "url": "/api/v1/cards/{:id}",
    "title": "4-Update an existing Card",
    "version": "0.3.0",
    "name": "PutCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/cards/6 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"value\": 400,\n  \"status\": 2,\n  \"expired_at\": \"2018-07-26\",\n  \"user_id\": 3\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card user ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 6,\n        \"number\": \"fckpzo7h3gtb94xq\",\n        \"value\": 200,\n        \"status\": 2,\n        \"expired_at\": \"2022-05-20\",\n        \"user_id\": 1,\n        \"invoice_id\": 0,\n        \"created_at\": \"2018-07-25T13:24:23.665Z\",\n        \"updated_at\": \"2018-07-25T13:24:23.665Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The id of the Card was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CardNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "post",
    "url": "/api/v1/send_card",
    "title": "8-Send Card",
    "version": "0.3.0",
    "name": "SendCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/send_card \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"card_id\": 10,\n  \"card_value\": 100,\n  \"user_email\": \"usereamil@gmail.com\",\n  \"card_number\": \"a90o-l7b6-hwrd-yj1e\",\n  \"admin_msg\": \"hello from payers\",\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>value card value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_id",
            "description": "<p>value card unique id.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "card_number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_email",
            "description": "<p>receiver user email.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "admin_msg",
            "description": "<p>message sent with user email.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "smstext",
            "description": "<p>message sent to user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"admin_note\": \"Message Was Not Sent to user\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardWasNotSent",
            "description": "<p>something went wrong while sending this card.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"error\": \"Card Was Not Sent to user\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_status?id={:id}",
    "title": "6-Suspend a Specific Card",
    "version": "0.3.0",
    "name": "SuspendCard",
    "group": "Cards",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_status?id=22",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>card status(1 for enable &amp; 4 for suspend).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "number",
            "description": "<p>card unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "value",
            "description": "<p>card value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "expired_at",
            "description": "<p>card expired Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "invoice_id",
            "description": "<p>card invoice ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": 1,\n  \"id\": 22,\n  \"number\": \"gos3ncliadx1jr54\",\n  \"value\": 200,\n  \"expired_at\": \"2018-07-31\",\n  \"card_type\": 1,\n  \"country_id\": 0,\n  \"user_id\": 1,\n  \"invoice_id\": 0,\n  \"created_at\": \"2018-07-28T10:07:53.960Z\",\n  \"updated_at\": \"2018-07-29T08:29:33.484Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The ID of this Card was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Card Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_controller.rb",
    "groupTitle": "Cards"
  },
  {
    "type": "Delete",
    "url": "/api/v1/cards_categories/{:id}",
    "title": "6-Delete an existing Card Category",
    "version": "0.3.0",
    "name": "DeleteCardCategory",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X DELETE \\\nhttp://localhost:3000/api/v1/cards_categories/5 \\",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card category unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"card_value\": 400,\n  \"active\": 1,\n  \"order\": 5,\n  \"created_at\": \"2018-07-28T12:37:11.483Z\",\n  \"updated_at\": \"2018-07-28T15:24:10.303Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Card Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response4:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CategoryNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_categories",
    "title": "1-Request cards categories List",
    "version": "0.3.0",
    "name": "GetCardsCategories",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_categories",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n{\n    \"id\": 1,\n    \"card_value\": 100,\n    \"active\": 1,\n    \"order\": 1,\n    \"created_at\": \"2018-07-26T14:41:50.506Z\",\n    \"updated_at\": \"2018-07-28T08:03:16.400Z\"\n},\n{\n  \"id\": 3,\n  \"card_value\": 300,\n  \"active\": 0,\n  \"order\": 3,\n  \"created_at\": \"2018-07-26T14:49:06.665Z\",\n  \"updated_at\": \"2018-07-28T08:38:59.747Z\"\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/display_cards_categories",
    "title": "2-Request displayed cards categories List",
    "version": "0.3.0",
    "name": "GetDisplayedCardsCategories",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/display_cards_categories",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n{\n    \"id\": 1,\n    \"card_value\": 100,\n    \"active\": 1,\n    \"order\": 1,\n    \"created_at\": \"2018-07-26T14:41:50.506Z\",\n    \"updated_at\": \"2018-07-28T08:03:16.400Z\"\n},\n{\n    \"id\": 2,\n    \"card_value\": 200,\n    \"active\": 1,\n    \"order\": 2,\n    \"created_at\": \"2018-07-26T14:48:50.514Z\",\n    \"updated_at\": \"2018-07-28T08:38:53.795Z\"\n}\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_categories/{:id}",
    "title": "3-Request Specific card category",
    "version": "0.3.0",
    "name": "GetSpecificCardsCategories",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_categories/3",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card Category unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 3,\n  \"card_value\": 300,\n  \"active\": 0,\n  \"order\": 3,\n  \"created_at\": \"2018-07-26T14:49:06.665Z\",\n  \"updated_at\": \"2018-07-28T08:38:59.747Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Card Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CategoryNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "post",
    "url": "/api/v1/cards_categories",
    "title": "4-Create a new Card Category",
    "version": "0.3.0",
    "name": "PostCardCategory",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/cards_categories \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"card_value\": 500,\n  \"active\": 1,\n  \"order\": 5,\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category unique order(high order for high category value).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"card_value\": 500,\n  \"active\": 0,\n  \"order\": 5,\n  \"created_at\": \"2018-07-28T12:37:11.483Z\",\n  \"updated_at\": \"2018-07-28T12:37:11.483Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingValue",
            "description": "<p>card category value has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingOrder",
            "description": "<p>card category order has already been taken.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Existing Category Value\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Order\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "post",
    "url": "/api/v1/cards_categories/{:id}",
    "title": "5-Update an existing Card Category",
    "version": "0.3.0",
    "name": "PutCardCategory",
    "group": "Cards_Categories",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/v1/cards_categories/5 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"card_value\": 400,\n  \"active\": 1,\n  \"order\": 5,\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category unique order(high order for high category value).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_value",
            "description": "<p>card category value.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>control card category display(1 for display &amp; 0 for hide).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "order",
            "description": "<p>category order(high order for high category value).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 5,\n  \"card_value\": 400,\n  \"active\": 1,\n  \"order\": 5,\n  \"created_at\": \"2018-07-28T12:37:11.483Z\",\n  \"updated_at\": \"2018-07-28T15:24:10.303Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingValue",
            "description": "<p>card category value has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingOrder",
            "description": "<p>card category order has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CategoryNotFound",
            "description": "<p>The id of the Card Category was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 Existing Category Value\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "HTTP/1.1 Invalid Order\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response4:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CategoryNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_categories_controller.rb",
    "groupTitle": "Cards_Categories"
  },
  {
    "type": "get",
    "url": "/api/v1/cards_logs",
    "title": "1-Request card log List",
    "version": "0.3.0",
    "name": "GetCardLog",
    "group": "Cards_Logs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_logs",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card log unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card unique user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "action_type",
            "description": "<p>card log type (1 for created ,2 for charged ,3 for deleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ip",
            "description": "<p>card log user interface ip.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_id",
            "description": "<p>card unique id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"user_id\": 1,\n      \"action_type\": 2,\n      \"ip\": \"127.0.0.1\",\n      \"created_at\": \"2018-09-09T08:10:43.950Z\",\n      \"updated_at\": \"2018-09-09T08:10:43.950Z\",\n      \"card_id\": 36\n  },\n  {\n      \"id\": 2,\n      \"user_id\": 1,\n      \"action_type\": 3,\n      \"ip\": \"127.0.0.1\",\n      \"created_at\": \"2018-09-09T08:13:16.518Z\",\n      \"updated_at\": \"2018-09-09T08:13:16.518Z\",\n      \"card_id\": 5\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_logs_controller.rb",
    "groupTitle": "Cards_Logs"
  },
  {
    "type": "get",
    "url": "/api/cards_logs/{:id}",
    "title": "2-Request Specific Card Log",
    "version": "0.3.0",
    "name": "GetSpecificCardLog",
    "group": "Cards_Logs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/cards_logs/2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>card log unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>card unique user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "action_type",
            "description": "<p>card log type (1 for created ,2 for charged ,3 for deleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "ip",
            "description": "<p>card log user interface ip.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "card_id",
            "description": "<p>card unique id.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 2,\n  \"user_id\": 1,\n  \"action_type\": 1,\n  \"ip\": \"127.0.0.1\",\n  \"created_at\": \"2018-09-09T07:52:48.179Z\",\n  \"updated_at\": \"2018-09-09T07:52:48.179Z\",\n  \"card_id\": 36\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardLogNotFound",
            "description": "<p>The id of the Card log was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"CardLogNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/cards_logs_controller.rb",
    "groupTitle": "Cards_Logs"
  },
  {
    "type": "delete",
    "url": "/countries/:id",
    "title": "Delete Country",
    "version": "0.3.0",
    "name": "DeleteCountry",
    "group": "Countries",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>Delete a Country.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X DELETE http://localhost:3000/api/v1/countries/2 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\"",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 Success\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only  authenticated admins can delete the country.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CountryNotFound",
            "description": "<p>The <code>id</code> of the Country was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"CountryNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "get",
    "url": "/api/v1/countries",
    "title": "List all countries",
    "version": "0.3.0",
    "name": "GetCountries",
    "group": "Countries",
    "description": "<p>get details of all countries.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/countries",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "get",
    "url": "/api/countries/{:id}",
    "title": "Get Country Data",
    "version": "0.3.0",
    "name": "GetCountry",
    "group": "Countries",
    "description": "<p>get details of specific country.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/countries/1",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CountryNotFound",
            "description": "<p>The <code>id</code> of the Country was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"CountryNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "put",
    "url": "/countries/:id/edit",
    "title": "Update Country",
    "version": "0.3.0",
    "name": "PutCountry",
    "group": "Countries",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>update data of a Country.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT http://localhost:3000/api/v1/countries/1 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ \"short_code\": \"test edit\"}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated admins can update the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CountryNotFound",
            "description": "<p>The <code>id</code> of the Country was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"CountryNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "post",
    "url": "/countries/",
    "title": "Create new Country",
    "version": "0.3.0",
    "name": "postCountry",
    "group": "Countries",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>create new Country.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:       ",
        "content": "curl -X POST  http://localhost:3000/api/v1/countries -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"country\": {\"short_code\": \"SAU\",\"Full_Name\": \"Saudi Arabia\t\",\"Phone_code\": \"00966\", \"Currency\": \"SAR\", \"language\": \"arabic\",\"active\": 1}}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated admin can create Countries.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid-token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "get",
    "url": "/api/v1/active_sessions?id={:user-id}",
    "title": "show user's active sessions",
    "version": "0.3.0",
    "name": "Getactivesessions",
    "group": "Log",
    "description": "<p>show user's active sessions.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/active_sessions?id=1 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo5fQ.xM_NrnhjjJejtZFatkVZWAZS_BFxvtjBns50IdcakzU\"",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "ipaddress",
            "description": "<p>Ip address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_agent",
            "description": "<p>Session Details\t.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>device id</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the session.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the session.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can perform this action.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/logins_controller.rb",
    "groupTitle": "Log"
  },
  {
    "type": "get",
    "url": "/api/v1/user_log?id={:user-id}",
    "title": "show user log",
    "version": "0.3.0",
    "name": "Getuserlog",
    "group": "Log",
    "description": "<p>show user log.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_log?id=1",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "ipaddress",
            "description": "<p>Ip address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "operation_type",
            "description": "<p>Details\t.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Log.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Log.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can perform this action.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/watchdogs_controller.rb",
    "groupTitle": "Log"
  },
  {
    "type": "get",
    "url": "/api/v1/money_ops",
    "title": "1-Request Money Operations List",
    "version": "0.3.0",
    "name": "GetMoneyOperation",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/money_ops",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"opid\": \"PYZPRV18M7NT\",\n      \"optype\": 1,\n      \"amount\": \"150\",\n      \"payment_gateway\": 1,\n      \"status\": 0,\n      \"payment_date\": \"2018-08-01T09:52:00.000Z\",\n      \"payment_id\": 3,\n      \"user_id\": 1,\n      \"created_at\": \"2018-08-01T09:52:20.462Z\",\n      \"updated_at\": \"2018-08-01T09:52:20.462Z\"\n  },\n  {\n      \"id\": 2,\n      \"opid\": \"PYTUGN80WB39\",\n      \"optype\": 1,\n      \"amount\": \"50\",\n      \"payment_gateway\": 1,\n      \"status\": 0,\n      \"payment_date\": \"2018-08-01T09:52:00.000Z\",\n      \"payment_id\": 2,\n      \"user_id\": 1,\n      \"created_at\": \"2018-08-01T09:52:56.776Z\",\n      \"updated_at\": \"2018-08-01T09:52:56.776Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "get",
    "url": "/api/money_ops/{:id}",
    "title": "2-Request Specific Money Operation",
    "version": "0.3.0",
    "name": "GetSpecificMoneyOperation",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/money_ops/2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card Operation ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 2,\n      \"opid\": \"PYTUGN80WB39\",\n      \"optype\": 1,\n      \"amount\": \"50\",\n      \"payment_gateway\": 1,\n      \"status\": 0,\n      \"payment_date\": \"2018-08-01T09:52:00.000Z\",\n      \"payment_id\": 2,\n      \"user_id\": 1,\n      \"created_at\": \"2018-08-01T09:52:56.776Z\",\n      \"updated_at\": \"2018-08-01T09:52:56.776Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The id of this Money Operation was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Card Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "post",
    "url": "/api/v1/money_ops",
    "title": "3-Create a new Money Operation",
    "version": "0.3.0",
    "name": "PostMoneyOperation",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/money_ops \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n\"optype\": 1,\n\"amount\": \"150\",\n\"payment_gateway\": 1,\n\"status\": 0,\n\"payment_date\": \"2018-08-01T13:31:00.000Z\",\n\"payment_id\": 3,\n\"user_id\": 1\n}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number (Created automatically and must be 12 digits and letters ).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 8,\n  \"opid\": \"PYL42W60CTGM\",\n  \"optype\": 1,\n  \"amount\": \"150\",\n  \"payment_gateway\": 1,\n  \"status\": 0,\n  \"payment_date\": \"2018-08-01T13:31:00.000Z\",\n  \"payment_id\": 3,\n  \"user_id\": 1,\n  \"created_at\": \"2018-08-01T11:33:49.055Z\",\n  \"updated_at\": \"2018-08-01T11:33:49.055Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingOperationId",
            "description": "<p>Operation Id has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 Existing Operation Id\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "get",
    "url": "/api/v1/operations_status?id={:id}",
    "title": "4-Update a Specific Operation Status",
    "version": "0.3.0",
    "name": "UpdateOperationStatus",
    "group": "Money_Operations",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/operations_status?id=5",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Money Operation unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "opid",
            "description": "<p>Operation unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "optype",
            "description": "<p>Operation type (1 for Sent &amp; 2 for Receive).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "amount",
            "description": "<p>Operation amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_gateway",
            "description": "<p>Operation payment gateway (paypal,bitcoin,...).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>Operation status (0 for pending &amp; 1 for compeleted).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "payment_date",
            "description": "<p>Payment operation creation date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "payment_id",
            "description": "<p>Payment ID for the operation itself.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Operation user.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": 1,\n  \"id\": 8,\n  \"opid\": \"PYL42W60CTGM\",\n  \"optype\": 1,\n  \"amount\": \"150\",\n  \"payment_gateway\": 1,\n  \"payment_date\": \"2018-08-01T13:31:00.000Z\",\n  \"payment_id\": 3,\n  \"user_id\": 1,\n  \"created_at\": \"2018-08-01T11:33:49.055Z\",\n  \"updated_at\": \"2018-08-01T15:45:10.000Z\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CardNotFound",
            "description": "<p>The ID of this Mony Operation was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Card Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/money_ops_controller.rb",
    "groupTitle": "Money_Operations"
  },
  {
    "type": "get",
    "url": "/api/v1/payers_blogs_categories",
    "title": "4-Request a list of all payers blogs categories",
    "version": "0.3.0",
    "name": "GetBlogsCategories",
    "group": "PayersBlogs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://payers2019.herokuapp.com/api/v1/payers_blogs_categories",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>category unique ID. (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>category description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"name\": \"PHP\",\n      \"description\": \"php laravel blog\",\n      \"created_at\": \"2019-01-24T11:00:22.763Z\",\n      \"updated_at\": \"2019-01-24T11:00:22.763Z\"\n  },\n  {\n      \"id\": 2,\n      \"name\": \"ROR\",\n      \"description\": \"ruby on rails blog\",\n      \"created_at\": \"2019-01-24T11:01:01.304Z\",\n      \"updated_at\": \"2019-01-24T11:01:01.304Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>no result found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"There are no categories.\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/payers_blogs_controller.rb",
    "groupTitle": "PayersBlogs"
  },
  {
    "type": "get",
    "url": "/api/v1/catblogs?user_id={:author_id}OR?cat_id={:category_id}",
    "title": "2-Request a list of payers blogs for a Specific author or category",
    "version": "0.3.0",
    "name": "GetCatBlogs",
    "group": "PayersBlogs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://payers2019.herokuapp.com/api/v1/catblogs?user_id=1\ncurl -i https://payers2019.herokuapp.com/api/v1/catblogs?cat_id=2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "author_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "category_id",
            "description": "<p>the blog category unique id.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>blog unique ID. (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "author_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>blog title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>blog slug.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>blog content.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>blog keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category_id",
            "description": "<p>the blog category unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>blog activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category_name",
            "description": "<p>blog category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 2,\n      \"author_id\": 2,\n      \"title\": \"test blog\",\n      \"slug\": \"مدونه\",\n      \"content\": \"شرح كيفيه الاشتراك ف بايزر وتحويل الاموال بكل سهوله ويسر\",\n      \"keywords\": \"new_account\",\n      \"category_id\": 1,\n      \"created_at\": \"2019-01-24T15:22:16.519Z\",\n      \"updated_at\": \"2019-01-24T15:22:16.519Z\",\n      \"active\": 1\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>no result found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"There are no results for this item.\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/payers_blogs_controller.rb",
    "groupTitle": "PayersBlogs"
  },
  {
    "type": "get",
    "url": "/api/v1/users_blogs",
    "title": "1-Request a list of all payers blogs",
    "version": "0.3.0",
    "name": "GetPayersBlogs",
    "group": "PayersBlogs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://payers2019.herokuapp.com/api/v1/users_blogs",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>blog unique ID. (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "author_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>blog title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>blog slug.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>blog content.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>blog keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category_id",
            "description": "<p>the blog category unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>blog activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category_name",
            "description": "<p>blog category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"author_id\": 1,\n      \"title\": \"اختبار المدونه\",\n      \"slug\": \"مدونه\",\n      \"content\": \"يجرى الان اختبار المدونه الخاصه بمهندسى بايرز\",\n      \"keywords\": \"test_blog\",\n      \"category_id\": 2,\n      \"created_at\": \"2019-01-24T11:22:16.519Z\",\n      \"updated_at\": \"2019-01-24T11:24:58.131Z\",\n      \"active\": 1,\n      \"category_name\": \"ROR\"\n  },\n  {\n      \"id\": 2,\n      \"author_id\": 2,\n      \"title\": \"test blog\",\n      \"slug\": \"مدونه\",\n      \"content\": \"شرح كيفيه الاشتراك ف بايزر وتحويل الاموال بكل سهوله ويسر\",\n      \"keywords\": \"new_account\",\n      \"category_id\": 1,\n      \"created_at\": \"2019-01-24T15:22:16.519Z\",\n      \"updated_at\": \"2019-01-24T15:22:16.519Z\",\n      \"active\": 1,\n      \"category_name\": \"PHP\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>no result found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"There are no Blogs.\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/payers_blogs_controller.rb",
    "groupTitle": "PayersBlogs"
  },
  {
    "type": "get",
    "url": "/api/v1/display_blog?id={:blog_id}",
    "title": "3-Request a Specific Blog",
    "version": "0.3.0",
    "name": "GetSpecificBlog",
    "group": "PayersBlogs",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i https://payers2019.herokuapp.com/api/v1/catblogs?id=1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>blog unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>blog unique ID. (Created automatically).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "author_id",
            "description": "<p>the user unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>blog title.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "slug",
            "description": "<p>blog slug.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "content",
            "description": "<p>blog content.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "keywords",
            "description": "<p>blog keywords.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "category_id",
            "description": "<p>the blog category unique id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "active",
            "description": "<p>blog activity (1 for enable , 0 for disable).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "category_name",
            "description": "<p>blog category name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"author_id\": 1,\n  \"title\": \"اختبار المدونه\",\n  \"slug\": \"مدونه\",\n  \"content\": \"يجرى الان اختبار المدونه الخاصه بمهندسى بايرز\",\n  \"keywords\": \"test_blog\",\n  \"category_id\": 2,\n  \"created_at\": \"2019-01-24T11:22:16.519Z\",\n  \"updated_at\": \"2019-01-24T11:24:58.131Z\",\n  \"active\": 1,\n  \"category_name\": \"ROR\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>no result found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"This Blog Was Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/payers_blogs_controller.rb",
    "groupTitle": "PayersBlogs"
  },
  {
    "type": "get",
    "url": "/api/ticket_departments/{:id}",
    "title": "3-Request Specific Ticket department",
    "version": "0.3.0",
    "name": "GetSpecificTicketDepartments",
    "group": "Ticket_departments",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/ticket_departments/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>department ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>department unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>department name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>department code.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>department description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n      \"id\": 1,\n      \"name\": \"department 1\",\n      \"code\": \"AS\",\n      \"description\": \"Description Description\",\n      \"created_at\": \"2018-09-08T12:22:09.000Z\",\n      \"updated_at\": \"2018-09-08T12:22:09.000Z\"\n     }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TicketDepartmentNotFound",
            "description": "<p>The id of the Ticket department was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TicketDepartmentNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/ticket_departments_controller.rb",
    "groupTitle": "Ticket_departments"
  },
  {
    "type": "get",
    "url": "/api/ticket_departments",
    "title": "1-Request ticket departments List",
    "version": "0.3.0",
    "name": "GetTicketDepartments",
    "group": "Ticket_departments",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/ticket_departments",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>department unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>department name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>department code.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>department description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n         \"id\": 1,\n         \"name\": \"department 1\",\n         \"code\": \"AS\",\n         \"description\": \"Description Description\",\n         \"created_at\": \"2018-09-08T12:22:09.000Z\",\n         \"updated_at\": \"2018-09-08T12:22:09.000Z\"\n     },\n     {\n         \"id\": 2,\n         \"name\": \"department 2\",\n         \"code\": \"AW\",\n         \"description\": \"Description Description\",\n         \"created_at\": \"2018-09-08T14:20:07.000Z\",\n         \"updated_at\": \"2018-09-08T14:20:07.000Z\"\n     }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/ticket_departments_controller.rb",
    "groupTitle": "Ticket_departments"
  },
  {
    "type": "post",
    "url": "/api/ticket_departments",
    "title": "2-Create a new ticket department",
    "version": "0.3.0",
    "name": "PostTicketDepartments",
    "group": "Ticket_departments",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/ticket_departments \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"name\": \"department 3\",\n  \"code\": \"AK\",\n  \"description\": \"Description 3\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>department name.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>department code.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>department description.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>department unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>department name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>department code.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>department description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 3,\n        \"name\": \"department 3\",\n        \"code\": \"AK\",\n        \"description\": \"Description 3\",\n        \"created_at\": \"2018-09-08T14:25:02.000Z\",\n        \"updated_at\": \"2018-09-08T14:25:02.000Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/ticket_departments_controller.rb",
    "groupTitle": "Ticket_departments"
  },
  {
    "type": "put",
    "url": "/api/ticket_departments/{:id}",
    "title": "4-Update an existing ticket department",
    "version": "0.3.0",
    "name": "PutTicketDepartments",
    "group": "Ticket_departments",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/ticket_departments/6 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"code\": \"AT\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>department code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>department unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>department name.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>department code.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>department description.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 3,\n        \"code\": \"AT\",\n        \"name\": \"department 3\",\n        \"description\": \"Description 3\",\n        \"created_at\": \"2018-09-08T14:25:02.000Z\",\n        \"updated_at\": \"2018-09-08T14:29:29.000Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TicketDepartmentNotFound",
            "description": "<p>The id of the Ticket department was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TicketDepartmentNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/ticket_departments_controller.rb",
    "groupTitle": "Ticket_departments"
  },
  {
    "type": "get",
    "url": "/api/tickets/{:id}",
    "title": "3-Request Specific Ticket",
    "version": "0.3.0",
    "name": "GetSpecificTicket",
    "group": "Tickets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/tickets/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Ticket ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Ticket unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "number",
            "description": "<p>Ticket unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>Ticket title.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Ticket content.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "attachment",
            "description": "<p>Ticket attachment.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ticket_id",
            "description": "<p>Ticket ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n     {\n       \"ticket\": {\n         \"id\": 3,\n         \"number\": \"AS\",\n         \"title\": \"Ticket 1\",\n         \"content\": \"Content\",\n         \"attachment\": \"Attachment\",\n         \"created_at\": \"2018-09-08T12:38:16.000Z\",\n         \"updated_at\": \"2018-09-08T12:38:16.000Z\"\n     },\n     \"ticket_reply\": [\n     {\n         \"id\": 1,\n         \"content\": \"content 1\",\n         \"created_at\": \"2018-09-10T14:16:36.000Z\",\n         \"updated_at\": \"2018-09-10T14:16:36.000Z\",\n         \"ticket_id\": 3\n     },\n     {\n         \"id\": 2,\n         \"content\": \"content 2\",\n         \"created_at\": \"2018-09-10T14:18:18.000Z\",\n         \"updated_at\": \"2018-09-10T14:18:18.000Z\",\n         \"ticket_id\": 3\n     },\n     {\n         \"id\": 3,\n         \"content\": \"content 3\",\n         \"created_at\": \"2018-09-10T14:19:49.000Z\",\n         \"updated_at\": \"2018-09-10T14:19:49.000Z\",\n         \"ticket_id\": 3\n     },\n     {\n         \"id\": 4,\n         \"content\": \"content 4\",\n         \"created_at\": \"2018-09-10T14:24:52.000Z\",\n        \"updated_at\": \"2018-09-10T14:24:52.000Z\",\n         \"ticket_id\": 3\n     }\n     ]\n     }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TicketNotFound",
            "description": "<p>The id of the Ticket was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TicketNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/tickets_controller.rb",
    "groupTitle": "Tickets"
  },
  {
    "type": "get",
    "url": "/api/tickets/{:id}/replies",
    "title": "4-Request Specific Ticket replies",
    "version": "0.3.0",
    "name": "GetSpecificTicketReplies",
    "group": "Tickets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/tickets/3/replies",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Ticket ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Reply unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Reply content.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "ticket_id",
            "description": "<p>Ticket ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n     {\n         \"id\": 1,\n         \"content\": \"content 1\",\n         \"created_at\": \"2018-09-10T14:16:36.000Z\",\n         \"updated_at\": \"2018-09-10T14:16:36.000Z\",\n         \"ticket_id\": 3\n     },\n     {\n         \"id\": 2,\n         \"content\": \"content 2\",\n         \"created_at\": \"2018-09-10T14:18:18.000Z\",\n         \"updated_at\": \"2018-09-10T14:18:18.000Z\",\n         \"ticket_id\": 3\n     },\n     {\n         \"id\": 3,\n         \"content\": \"content 3\",\n         \"created_at\": \"2018-09-10T14:19:49.000Z\",\n         \"updated_at\": \"2018-09-10T14:19:49.000Z\",\n         \"ticket_id\": 3\n     },\n     {\n         \"id\": 4,\n         \"content\": \"content 4\",\n         \"created_at\": \"2018-09-10T14:24:52.000Z\",\n        \"updated_at\": \"2018-09-10T14:24:52.000Z\",\n         \"ticket_id\": 3\n     }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TicketNotFound",
            "description": "<p>The id of the Ticket was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TicketNotFound\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/tickets_controller.rb",
    "groupTitle": "Tickets"
  },
  {
    "type": "get",
    "url": "/api/tickets",
    "title": "1-Request tickets List",
    "version": "0.3.0",
    "name": "GetTicket",
    "group": "Tickets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/tickets",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Ticket unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "number",
            "description": "<p>Ticket unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>Ticket title.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Ticket content.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "attachment",
            "description": "<p>Ticket attachment.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n     {\n        \"id\": 3,\n        \"number\": \"AS\",\n        \"title\": \"Ticket 1\",\n        \"content\": \"Content\",\n        \"attachment\": \"Attachment\",\n        \"created_at\": \"2018-09-08T12:38:16.000Z\",\n        \"updated_at\": \"2018-09-08T12:38:16.000Z\"\n    },\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/tickets_controller.rb",
    "groupTitle": "Tickets"
  },
  {
    "type": "post",
    "url": "/api/tickets",
    "title": "2-Create a new ticket",
    "version": "0.3.0",
    "name": "PostTicket",
    "group": "Tickets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/tickets \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"number\": \"AB\",\n  \"title\": \"Ticket 2\",\n  \"content\": \"Content 2\",\n  \"attachment\": \"Attachment 2\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "number",
            "description": "<p>Ticket unique number.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>Ticket title.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Ticket content.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "attachment",
            "description": "<p>Ticket attachment.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Ticket unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "number",
            "description": "<p>Ticket unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>Ticket title.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Ticket content.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "attachment",
            "description": "<p>Ticket attachment.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 4,\n        \"number\": \"AB\",\n        \"title\": \"Ticket 2\",\n        \"content\": \"Content 2\",\n        \"attachment\": \"Attachment 2\",\n        \"created_at\": \"2018-09-08T13:05:09.000Z\",\n        \"updated_at\": \"2018-09-08T13:05:09.000Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/tickets_controller.rb",
    "groupTitle": "Tickets"
  },
  {
    "type": "post",
    "url": "/api/tickets",
    "title": "5-Create a new ticket reply",
    "version": "0.3.0",
    "name": "PostTicketReply",
    "group": "Tickets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/tickets/reply \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"content\": \"content5\",\n  \"ticket_id\": 3\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Reply content.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "ticket_id",
            "description": "<p>Ticket ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Reply unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Reply content.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ticket_id",
            "description": "<p>Ticket ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n         \"id\": 11,\n         \"content\": \"content5\",\n         \"created_at\": \"2018-09-12T13:15:58.000Z\",\n         \"updated_at\": \"2018-09-12T13:15:58.000Z\",\n         \"ticket_id\": 3\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/tickets_controller.rb",
    "groupTitle": "Tickets"
  },
  {
    "type": "put",
    "url": "/api/tickets/{:id}",
    "title": "6-Update an ticket",
    "version": "0.3.0",
    "name": "PutTicket",
    "group": "Tickets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT \\\nhttp://localhost:3000/api/tickets/6 \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n  \"title\": \"Ticket 3\",\n  \"content\": \"Content 3\",\n  \"attachment\": \"Attachment 3\"\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>Ticket title.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Ticket content.</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "attachment",
            "description": "<p>Ticket attachment.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Ticket unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "number",
            "description": "<p>Ticket unique number.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "title",
            "description": "<p>Ticket title.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "content",
            "description": "<p>Ticket content.</p>"
          },
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "attachment",
            "description": "<p>Ticket attachment.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n    {\n        \"id\": 4,\n        \"title\": \"Ticket 3\",\n        \"content\": \"Content 3\",\n        \"attachment\": \"Attachment 3\",\n        \"number\": \"AB\",\n        \"created_at\": \"2018-09-08T13:05:09.000Z\",\n        \"updated_at\": \"2018-09-08T13:10:00.000Z\"\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>missing user name or password.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "TicketNotFound",
            "description": "<p>The id of the Ticket was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"TicketNotFound\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/tickets_controller.rb",
    "groupTitle": "Tickets"
  },
  {
    "type": "get",
    "url": "/api/v1/lock_unlock_account?id={:user-id}",
    "title": "change account status",
    "version": "0.3.0",
    "name": "GetAccountStatus",
    "group": "User",
    "description": "<p>change account status for user.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/lock_unlock_account?id=1",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"mseeage\": \"user has been enabled/disabled\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can perform this action.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/users/{:id}",
    "title": "Get User Data",
    "version": "0.3.0",
    "name": "GetUser",
    "group": "User",
    "description": "<p>get details of specific user.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/users/1",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"UserNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/v1/users",
    "title": "List all users",
    "version": "0.3.0",
    "name": "GetUsers",
    "group": "User",
    "description": "<p>get details of all users.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMn0.HjzZehfkPaDES35pXYwdlXNydDMWhwayCjMBs5KUcOE\" -i http://localhost:3000/api/v1/users",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>User's-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "uuid",
            "description": "<p>User's-uuid.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "account_number",
            "description": "<p>User's account_number.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>User's telephone.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "roleid",
            "description": "<p>User's role id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>User's first name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User's last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "disabled",
            "description": "<p>User's account status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Email confirmation status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "loginattempts",
            "description": "<p>User's login attempts.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "failedattempts",
            "description": "<p>User's failed attempts.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>User's account currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>User's country id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n\"id\": 1,\n \"uuid\": \"8dddd2c5\",\n \"roleid\": 1,\n \"username\": \"amira\",\n \"firstname\": \"amira\",\n \"lastname\": \"amira2\",\n \"disabled\": 1,\n \"loginattempts\": 7,\n \"status\": 1,\n \"created_at\": \"2018-07-30T13:19:38.031Z\",\n \"updated_at\": \"2018-08-02T13:02:55.156Z\",\n \"email\": \"amira.elfayhhome@servicehigh.com\",\n \"failedattempts\": 0,\n \"account_number\": \"PS100000001\",\n \"telephone\": \"00201007460059\",\n \"account_currency\": 0,\n \"country_id\": 1\n},\n{\n\"id\": 2,\n \"uuid\": \"dd89adb1\",\n \"roleid\": 1,\n \"username\": \"amira2\",\n \"firstname\": \"amira2\",\n \"lastname\": \"amira2\",\n \"disabled\": 1,\n \"loginattempts\": 0,\n \"status\": 0,\n \"created_at\": \"2018-07-30T13:19:58.031Z\",\n \"updated_at\": \"2018-08-02T13:02:55.156Z\",\n \"email\": \"amira.elfayhhome@servicehigh.com\",\n \"failedattempts\": 0,\n \"account_number\": \"PS100000002\",\n \"telephone\": \"0020102982852\",\n \"account_currency\": 0,\n \"country_id\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/v1/statistics?&account_status={:search_status}",
    "title": "Get Users Statistics",
    "version": "0.3.0",
    "name": "GetUsersstatistics",
    "group": "User",
    "description": "<p>get statistics of all users.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i 'http://localhost:3000/api/v1/statistics?&account_status=confirmed'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/v1/confirmmail",
    "title": "confirm user mail",
    "version": "0.3.0",
    "name": "POSTconfirmmail",
    "group": "User",
    "description": "<p>confirm user mail after registeration.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST  http://localhost:3000/api/v1/confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"email\": \"engamira333@gmail.com\",\"token\": \"3aa4e716\"}'",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"result : true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing",
            "description": "<p>wrong code .</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing2",
            "description": "<p>wrong email .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Badrequest\n {\n   \"result : false\"\n   \"error\": \"MissingData\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/v1/resend_confirmmail",
    "title": "resend confirmation mail",
    "version": "0.3.0",
    "name": "POSTresend_confirmmail",
    "group": "User",
    "description": "<p>resend confirmation mail to user after registeration.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST  http://localhost:3000/api/v1/resend_confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"email\": \"engamira333@gmail.com\"}'",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"result : true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing",
            "description": "<p>wrong email .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Badrequest\n {\n   \"result : false\"\n   \"error\": \"MissingData\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/users/:id/edit",
    "title": "Update User",
    "version": "0.3.0",
    "name": "PutUser",
    "group": "User",
    "description": "<p>update details of a user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "secret_code",
            "description": "<p>Users Secret Code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>Users telephone</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>Users currency</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "active_otp",
            "description": "<p>Users active two factor authentication</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT http://localhost:3000/api/v1/users/10 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ \"email\": \"amira@ggggg.com\"}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "secret_code",
            "description": "<p>Users Secret Code</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>Users telephone</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>Users currency</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active_otp",
            "description": "<p>Users active two factor authentication</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can update the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"UserNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/",
    "title": "Create new User",
    "version": "0.3.0",
    "name": "postUser",
    "group": "User",
    "description": "<p>create new user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>User's First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User's Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>User's Country</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's Username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's Password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "refered_by",
            "description": "<p>Invitation Code Of the Invitor</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:       ",
        "content": "curl -X POST  http://localhost:3000/api/v1/users -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"user\": {\"firstname\": \"amira2\",\"lastname\": \"amira2\",\"password\": \"Amira123456\", \"country_id\": 1,\"email\": \"tessst@gmail.com\",\"username\": \"EngineeraAmira2\", \"refered_by\": \"71000e83\"}}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "secret_code",
            "description": "<p>Users Secret Code</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>Users telephone</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>Users currency</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active_otp",
            "description": "<p>Users active two factor authentication</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can create User.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid-token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/user_wallets/{:id}",
    "title": "3-Request Specific User Wallet",
    "version": "0.3.0",
    "name": "GetSpecificGetUserWallet",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_wallets/1",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>Card Operation ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 1,\n      \"currency\": \"USD\",\n      \"amount\": 1000,\n      \"user_id\": 1,\n      \"status\": 1,\n      \"uuid\": \"0fa993bcfa59\",\n      \"created_at\": \"2018-08-02T11:03:14.634Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.183Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WalletNotFound",
            "description": "<p>The id of this User Wallet was not found.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"Wallet Not Found\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/user_wallets",
    "title": "1-Request Users Wallets List",
    "version": "0.3.0",
    "name": "GetUserWallets",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_wallets",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n      \"id\": 1,\n      \"currency\": \"USD\",\n      \"amount\": 1000,\n      \"user_id\": 1,\n      \"status\": 1,\n      \"uuid\": \"0fa993bcfa59\",\n      \"created_at\": \"2018-08-02T11:03:14.634Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.183Z\"\n  },\n  {\n      \"id\": 2,\n      \"currency\": \"USD\",\n      \"amount\": 500,\n      \"user_id\": 2,\n      \"status\": 1,\n      \"uuid\": \"0b51db6a5988\",\n      \"created_at\": \"2018-08-02T11:03:52.375Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.179Z\"\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid Token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 400 Bad Request\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "post",
    "url": "/api/v1/user_wallets",
    "title": "2-Create a new User Wallet",
    "version": "0.3.0",
    "name": "PostUserWallet",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST \\\nhttp://localhost:3000/api/v1/user_wallets \\\n-H 'cache-control: no-cache' \\\n-H 'content-type: application/json' \\\n-d '{\n\"amount\": \"500\",\n\"user_id\": 3,\n\"status\": 1\n}'",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID (unique).</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 3,\n      \"currency\": \"USD\",\n      \"amount\": 500,\n      \"user_id\": 3,\n      \"status\": 1,\n      \"uuid\": \"0fa663bcsa47\",\n      \"created_at\": \"2018-08-02T15:03:04.333Z\",\n      \"updated_at\": \"2018-08-02T15:03:04.383Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "ExistingUserId",
            "description": "<p>User Id has already been taken.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid token .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "HTTP/1.1 Existing User Id\n  {\n    \"error\": \"has already been taken\"\n  }",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "HTTP/1.1 422 Missing token\n  {\n    \"error\": \"Missing token\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/send_sms?tel={phone_number}",
    "title": "6-Send Verification SMS Code to Users",
    "version": "0.3.0",
    "name": "SendSmsCode",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/send_sms?tel=2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "tel",
            "description": "<p>User phone number + country code.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "tel",
            "description": "<p>User phone number.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    \"success\": \" Code was send successfully to num 201064****** \"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "SendCodeFailed",
            "description": "<p>Something went wrong while sending SMS code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"error\": \"Something wrong, Sending code was failed\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/transfer_balance?=&userfrom={:sender_id}&userto={:receiver_id}&transferamount={:amount}",
    "title": "5-Transfer Balances Between Users",
    "version": "0.3.0",
    "name": "TransferBalance",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/transfer_balance?=&userfrom=1&userto=2&transferamount=10",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userfrom",
            "description": "<p>the sender user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "userto",
            "description": "<p>the receiver user id.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "transferamount",
            "description": "<p>the amount transferred between users.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"success\": \"Balance was successfully transferred\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response1:",
          "content": "{\n  \"error\": \"You don't have enough money for this operation\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": "{\n  \"error\": \"Sorry, The transferd balance must be greater than 2 USD\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response3:",
          "content": "{\n  \"error\": \"Sorry, The transferd balance must be less or equal than 10 USD\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response4:",
          "content": "{\n  \"error\": \"Sorry, Your Wallet was Disabled\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response5:",
          "content": "{\n  \"error\": \"The sender User Wasn't Stored in Our Database\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response6:",
          "content": "{\n  \"error\": \"The receiver User Wasn't Stored in Our Database\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response6:",
          "content": "{\n  \"error\": \"Sorry, The Receiver User Wallet was Disabled\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response8:",
          "content": "{\n  \"error\": \"You can't transfer balance to yourself\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/wallets_status?id={:id}",
    "title": "4-Update a Specific User Wallet Status",
    "version": "0.3.0",
    "name": "UpdateWalletsStatus",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/wallets_status?id=2",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "status",
            "description": "<p>User Wallet (0 for Disabled &amp; 1 for Enabled).</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>User Wallet unique ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currency",
            "description": "<p>User Wallet Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "amount",
            "description": "<p>User Wallet amount.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "user_id",
            "description": "<p>Wallet User ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uuid",
            "description": "<p>User Wallet Secure random number (Created automatically and must be 12 digits and letters).</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date created.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date Updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n      \"id\": 2,\n      \"currency\": \"USD\",\n      \"amount\": 500,\n      \"user_id\": 2,\n      \"status\": 0,\n      \"uuid\": \"0b51db6a5988\",\n      \"created_at\": \"2018-08-02T11:03:52.375Z\",\n      \"updated_at\": \"2018-08-02T13:51:18.179Z\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "WalletNotFound",
            "description": "<p>The id of this User Wallet was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n  {\n    \"error\": \"User Wallet Not Found\"\n  }",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "type": "get",
    "url": "/api/v1/verifysms?code={SMS_code}",
    "title": "7-Verify SMS Code",
    "version": "0.3.0",
    "name": "VerifySMSCode",
    "group": "Users_Wallets",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/verifysms?code=3660",
        "type": "json"
      }
    ],
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "code",
            "description": "<p>The SMS verification code.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n    \"success\": \"كود صحيح , تم التفعيل بنجاح\"\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "InvalidCode",
            "description": "<p>The verification code was wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n  \"error\": \"الكود خطأ حاول مرة اخرى\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers_doc/app/controllers/api/v1/user_wallets_controller.rb",
    "groupTitle": "Users_Wallets"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/api/main.js",
    "group": "_home_ahmed_cardds_payers_doc_public_api_main_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_api_main_js",
    "name": ""
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-canvas-to-blob/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_canvas_to_blob_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-load-image/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_load_image_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/expect.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_expect_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/blueimp-tmpl/test/vendor/mocha.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_blueimp_tmpl_test_vendor_mocha_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/dist/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_dist_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/switchery/switchery.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_switchery_switchery_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/dist/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_dist_transitionize_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_transitionize_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/bower_components/transitionize/transitionize.js",
    "group": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_transitionize_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_bower_components_transitionize_transitionize_js",
    "name": "Public"
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "payers_doc/public/main.js",
    "group": "_home_ahmed_cardds_payers_doc_public_main_js",
    "groupTitle": "_home_ahmed_cardds_payers_doc_public_main_js",
    "name": ""
  }
] });
