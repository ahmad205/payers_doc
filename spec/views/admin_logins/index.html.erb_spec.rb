require 'rails_helper'

RSpec.describe "admin_logins/index", type: :view do
  before(:each) do
    assign(:admin_logins, [
      AdminLogin.create!(
        :admin => nil,
        :ip_address => "Ip Address",
        :user_agent => "User Agent",
        :device_id => "Device",
        :operation_type => "Operation Type"
      ),
      AdminLogin.create!(
        :admin => nil,
        :ip_address => "Ip Address",
        :user_agent => "User Agent",
        :device_id => "Device",
        :operation_type => "Operation Type"
      )
    ])
  end

  it "renders a list of admin_logins" do
    render
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Ip Address".to_s, :count => 2
    assert_select "tr>td", :text => "User Agent".to_s, :count => 2
    assert_select "tr>td", :text => "Device".to_s, :count => 2
    assert_select "tr>td", :text => "Operation Type".to_s, :count => 2
  end
end
