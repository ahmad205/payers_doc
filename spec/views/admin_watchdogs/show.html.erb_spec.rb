require 'rails_helper'

RSpec.describe "admin_watchdogs/show", type: :view do
  before(:each) do
    @admin_watchdog = assign(:admin_watchdog, AdminWatchdog.create!(
      :admin_id => 2,
      :ipaddress => "MyText",
      :operation_type => "Operation Type"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Operation Type/)
  end
end
