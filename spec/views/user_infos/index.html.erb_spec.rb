require 'rails_helper'

RSpec.describe "user_infos/index", type: :view do
  before(:each) do
    assign(:user_infos, [
      UserInfo.create!(
        :user_id => 2,
        :mobile => "Mobile",
        :address_verification_id => 3,
        :nationalid_verification_id => 4,
        :nationalid_verification_id => 5,
        :selfie_verification => 6,
        :status => 7
      ),
      UserInfo.create!(
        :user_id => 2,
        :mobile => "Mobile",
        :address_verification_id => 3,
        :nationalid_verification_id => 4,
        :nationalid_verification_id => 5,
        :selfie_verification => 6,
        :status => 7
      )
    ])
  end

  it "renders a list of user_infos" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Mobile".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.to_s, :count => 2
    assert_select "tr>td", :text => 6.to_s, :count => 2
    assert_select "tr>td", :text => 7.to_s, :count => 2
  end
end
