require 'rails_helper'

RSpec.describe "company_banks/show", type: :view do
  before(:each) do
    @company_bank = assign(:company_bank, CompanyBank.create!(
      :bank_key => "Bank Key",
      :bank_name => "Bank Name",
      :encryptkey => "Encryptkey",
      :currency => "Currency",
      :country => "Country",
      :city => "City",
      :branch => "Branch",
      :branch_code => "Branch Code",
      :phone => "Phone",
      :account_name => "Account Name",
      :account_email => "Account Email",
      :account_number => "Account Number",
      :swiftcode => "Swiftcode",
      :ibancode => "Ibancode",
      :visa_cvv => 2,
      :bank_category => "Bank Category",
      :bank_subcategory => "Bank Subcategory",
      :fees => 3.5,
      :ratio => 4.5,
      :logo => "Logo",
      :expire_at => ""
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Bank Key/)
    expect(rendered).to match(/Bank Name/)
    expect(rendered).to match(/Encryptkey/)
    expect(rendered).to match(/Currency/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/City/)
    expect(rendered).to match(/Branch/)
    expect(rendered).to match(/Branch Code/)
    expect(rendered).to match(/Phone/)
    expect(rendered).to match(/Account Name/)
    expect(rendered).to match(/Account Email/)
    expect(rendered).to match(/Account Number/)
    expect(rendered).to match(/Swiftcode/)
    expect(rendered).to match(/Ibancode/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Bank Category/)
    expect(rendered).to match(/Bank Subcategory/)
    expect(rendered).to match(/3.5/)
    expect(rendered).to match(/4.5/)
    expect(rendered).to match(/Logo/)
    expect(rendered).to match(//)
  end
end
