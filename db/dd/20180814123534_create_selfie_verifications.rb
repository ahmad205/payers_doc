class CreateSelfieVerifications < ActiveRecord::Migration[5.2]
  def change
    create_table :selfie_verifications do |t|
      t.integer :user_id
      t.string :note

      t.timestamps
    end
  end
end
